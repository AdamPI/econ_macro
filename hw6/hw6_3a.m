% logging
diary hw6_3a.log;

A = [0.1 0.3 0.6; 0.7 0 0.3; 0.1 0.5 0.4];
% display matrix
disp("-------------------------------------------------------------------")
disp("matrix");
disp("-------------------------------------------------------------------")
disp(A);
% calculate eigen value and eigen vectors of matrix
[eigen_vector, eigen_value] = eig(A);
% display eigen value
disp("-------------------------------------------------------------------")
disp("eigen value");
disp("-------------------------------------------------------------------")
disp(eigen_value);
% it turns out only the 3rd eigen value is real, display eigen vector of
% the corresponding eigen-value
magic_idx = 3;
ep = eigen_vector(:, magic_idx);
disp("-------------------------------------------------------------------")
disp(['it turns out only the 3rd eigen value is real, display eigen vector' newline ...
    'of the corresponding eigen value, which is the 3rd']);
disp("-------------------------------------------------------------------")
disp(ep);
% check A*ep == ev*ep
disp("-------------------------------------------------------------------")
disp("check A*ep == ev*ep");
disp("-------------------------------------------------------------------")
disp(A*ep);
disp("-------------------------------------------------------------------")
disp("normalize eigen value, e.g. sum_i{pi_i}=1");
disp("-------------------------------------------------------------------")
v_sum = sum(ep);
ep = ep ./ sum(ep);
disp(ep);
% turn off diary
diary off;